<img src="others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# Assignment 2: DNS Reflection Mitigation using P4 and INT

DNS reflection/amplification attacks are a specific kind of DDoS attack that uses DNS servers to flood a victim service or host with many DNS reply packets. In this assignment, you will learn a technique to detect and mitigate DNS reflection attacks. You will apply this technique on an emulated Mininet network, using P4 programmable switches, in-band telemetry (INT), and ONOS.

**You may work in a team of TWO if you would like (this is strongly suggested by not required; grading will only be based on your submission regardless of the size of the team).**

## Background

In a DNS reflection attack, an attacker sends a DNS request to a DNS server with the source IP address spoofed to the victim's IP address. The DNS server then sends the reply to the victim. The attacker can use many compromised machines (botnets) to generate a vast number of such requests, which results in an overwhelming amount of traffic (DNS responses) headed towards the victim's machine.

In addition to this reflection component, the attack also involves "amplification." The size of each DNS response is typically larger than the size of each corresponding request. As a result, even if the attacker launches the attack from a single compromised machine, the attack will result in more traffic (by bytes) reaching the victim than was sent by the attacker. The amplification effect, and therefore the power of the attack, becomes even more pronounced when the attack is launched from many machines.

### a. Detection

One way to detect DNS reflection attacks is to keep track of the DNS requests and responses that each host sends and receives. Suppose a programmable switch sends a copy of a DNS packet that it sees to a monitoring service, running alongside the network controller. The service can record, for each host, the identification number of each outgoing DNS query. If a DNS response's identification number does not match any of the at-large requests recorded for its destination, the service will increment a counter. If the counter passes some threshold, the service may determine that the host is receiving unsolicited responses and is thus a victim of a reflection attack.

### b. Mitigation

One way to mitigate DNS reflection is to identify and drop the attack traffic at the switch. More specifically, in the scenario described above, once the service detects an attack towards a specific host, it can ...

1. Install a rule on the switch to drop all DNS **responses** destined to the victim host, exceeding the threshold.
2. Install a higher priority rule to allow only those **responses** that match the ID of the legitimate DNS **queries**, originating from hosts.

## Getting Started

### Part A: Virtual environment and network topology
This assignment builds on [`assignment0`](../assignment0), so please ensure that you have a successfully working virtual environment. We will be building a slightly different network this time, as depicted in the following figure.

<img src="../../others/images/assg2-topo.png" alt="Network Topology." width="500"><br>

You can start the environment (i.e., Mininet, controller, etc.) using the same commands and steps listed in [`assignment0`](../assignment0), except `make netcfg`. 

Until now, we have been using the `basic.p4` program to configure our switch, which comes pre-packaged with the ONOS controller. This time, however, we will be using a custom P4 program (located under the [`p4src`](p4src) directory) to configure the switch, as well as a custom ONOS application (located under the [`app`](app) directory) to allow the controller to interact with switch `s1` running the new P4 program.

So, before executing the `make netcfg` command, follow these steps:

- Compile the P4 program. In the shell, run ...

```
$ cd assignments/assignment2
$ make build-p4
```

Doing so will compile the P4 program and store the output under the [`resources`](app/src/main/resources) folder in the [`app`](app) directory. 

- Next, compile the ONOS application. Run ...

```
$ cd assignments/assignment2
$ make build-app
```

Doing so will compile the ONOS application and create a self-contained `*.oar` file, under the [`app`](app) directory, containing both the P4 files and the application content. 

- To load the application to the controller, run ...

```
$ cd assignments/assignment2
$ make reload-app
```

You will see the following output on the terminal, where the ONOS controller is running:

```
02:36:57.777 INFO  [ApplicationManager] Application org.dataplane.pipelines.forward has been activated
```

Now, once the application is loaded, go ahead and run `make netcfg`. This will inform the controller to use the new application (and the P4 program) when connecting to switch `s1`, listening at the specific ip:port. 

> **Note:** You can verify the setup by pinging hosts, `h1` and `h2`.

### Part B: Generating DNS reflection attacks

In this setup, `h2` and `h3` constitute your private network through `s1`. `m1` is the monitoring service connected to `s1`, which will listen to all the incoming and outgoing DNS traffic of your network. `h1` is a host that is running `bind`, an open DNS resolver, and `h4` is an attacker.

You can start this setup by running the following command in a new terminal:

```
$ cd assignments/assignment2
$ make start-net
```

> **Note:** Give it a minute or so to finish.

Once started, `h2` and `h3` will begin sending DNS requests every 5 seconds and pings every 2 seconds to the DNS resolver `h1`. At the same time, `h4` will start sending spoofed DNS requests on behalf of `h2` to the DNS resolver every half a second. Thus, `h2` is going to be the victim of a DNS reflection attack.

Under the [`log`](log) folder, you will see that the `h2_ping.txt` and `h3_ping.txt` files have a record of successful pings, and the `h2_test.txt` and `h3_test.txt` files have a record of DNS responses and pings. 

After letting the setup run for a couple of minutes, you can then plot a time-series of DNS response counts using the following command:

```
$ cd assignments/assignment2
$ make plot-results
```

Doing so will generate a plot, named `DNS_response_rates.png`, under the [`log`](log) directory. Examining the plot, you will see that `h2`, the victim host, receives a lot more DNS responses than `h3`.

### Part C: Cloning, detection, and mitigation

First, the monitoring service, running on `m1`, needs switch `s1` to send a copy of each incoming and outgoing DNS packet for analysis. To do so, you have to complete the code in the [`monitor.p4`](p4src/include/monitor.p4) file as well as [`PipelinerImpl.java`](app/src/main/java/org/dataplane/pipelines/forward/PipelinerImpl.java) file of the ONOS application.

Next, the monitoring service needs to analyze DNS traffic and install flow rules to drop attack packets. The monitoring service runs the Python script, [`start_dns_monitor.py`](pysrc/start_dns_monitor.py). This script uses a Python library, called [Scapy](https://scapy.net/), to sniff packet from the network interface, connected to `s1`, applies the [`handle_packet()`](pysrc/start_dns_monitor.py#L33) method to each packet, and install rules to `s1` to allow or drop DNS traffic. You have to add code for the following:

- Install rules in switch `s1` to clone all incoming and outgoing DNS traffic to the monitoring service.
- Implement the DNS reflection attack detection and mitigation strategy described in the [Background](#background) section above.

> **Note:** Do not modify any existing code in `monitor.p4`, `PipelinerImpl.java`, or `start_dns_monitor.py` because it is necessary for correct packet processing.

#### a. Cloning

To clone packets to the monitoring service `m1` from switch `s1`, you have to ...

- Complete the `clone_packet` action in the [`monitor.p4`](p4src/include/monitor.p4) file (see [TODO](p4src/include/monitor.p4#L39)). Later, via the monitoring service `m1`, you will install rules with this action.
> **Note:** You can use the P4 `clone()' primitive for copying a packet (https://github.com/p4lang/p4c/blob/master/p4include/v1model.p4#L594). It takes two arguments:
> - type (use **`CloneType.I2E`**)
> - session ID (use **`500`**, which is configured to clone a copy of the packet to port **`5`**, connected to `m1'.)
>
> What this does is it tells the switch to generate and send a copy of a packet, arriving at the ingress pipeline (the I in I2E), to port (5) of the egress pipeline (the E in I2E) using a session ID (500).

- Add code in the [`PipelinerImpl.java`](app/src/main/java/org/dataplane/pipelines/forward/PipelinerImpl.java) file of the ONOS application (see [TODO](app/src/main/java/org/dataplane/pipelines/forward/PipelinerImpl.java#L89)) to install a new multicast group (or session) to instruct the traffic manager inside `s1` to clone a copy of the packet, whenever a rule with the `clone_packet` action is hit.
> **Note:** For an example on how to install a multicast rule from the ONOS app, see [here](https://github.com/opennetworkinglab/onos-p4-tutorial/blob/master/app/src/main/java/org/p4/p4d2/tutorial/pipeconf/PipelinerImpl.java#L117). Notice that session ID and port number in your case are **`500`** and **`5`**, respectively.

#### b. Detection

For DNS reflection detection, check each packet arriving at the monitoring service to see if it is a DNS request. If it is, record a mapping from the request's DNS identification number to its source IP.

On the other hand, for each DNS response packet, check whether there has been a request with the same identification number from the destination IP of the response. Keep track of the total number of unmatched responses sent to each host. If this number passes **50** for any host, mitigation should be started for that host (see [Mitigation](#mitigation-1) below).

There are three TODO sections in [`start_dns_monitor.py`](pysrc/start_dns_monitor.py). The [first](pysrc/start_dns_monitor.py#L43) is in the `if __name__ == "__main__":` section. This is where you have to call `rest.install_rule()` to install rules in the `monitor` table of switch `s1`, via a REST API, to clone DNS packets to the monitoring service. 

> **Note:** You can find more information on the `rest` library and examples, [here](pysrc/utils/rest.py#L321). **Remember that you will be installing rules in the `monitor` table in switch `s1`.**

The [second](pysrc/start_dns_monitor.py#L16) is in the constructor `__init__()`. This is where you should create any instance variables needed for the detection algorithm. These instance variables should start with the keyword `self`, (e.g., `self.id_to_host`). 

> **Note:** For more information on Python classes, see the documentation here: [A first look at classes](https://docs.python.org/2.7/tutorial/classes.html#a-first-look-at-classes).

The [third](pysrc/start_dns_monitor.py#L32) one is in the `handle_packet()` method. Here, you will need to update the instance variables in response to the current packet contents. The `pkt` argument is the current packet encoded in the Scapy packet format. The following example shows how to access the source and destination IP addresses of `pkt`:

```
# Check whether pkt is an IP packet
if IP in pkt:

  # Get source IP addresses
  src_ip = pkt[IP].src

  # Get destionation IP addresses
  dst_ip = pkt[IP].dst
```

The following example shows how to check whether a DNS packet is a request or a response and how to get its ID:

```
# Check whether pkt is a DNS packet
if DNS in pkt:

  # Check if pkt is a DNS response or request
  is_response = pkt[DNS].qr == 1
  is_request = pkt[DNS].qr == 0

  # Get ID of DNS request/response
  dns_id = pkt[DNS].id
```

> **Note:** [Scapy](https://scapy.net/) is a powerful packet manipulation tool that allows you to do more than just inspect packets (although that's all you will be using it for in this assignment). For more information about Scapy, see the documentation here: https://www.secdev.org/projects/scapy/doc/usage.html.

#### c. Mitigation

Once you detect that a host is being attacked, begin dropping all DNS response traffic destined to that specific host, except the ones for which the monitoring service has seen a legitimate query originating from that host.

Implement packet drops by installing a flow rule to switch `s1` when the unmatched responses go beyond the specified threshold (i.e., 50) for the victim host.

Moreover, to allow legitimate responses to pass through, install a flow rule whenever you see a new DNS query from a host and delete it when you see the corresponding response from the DNS resolver.

> **Note:** Although in this specific setup, `h2` is the victim, we may test your assignment using `h3` as a victim. Therefore, your detection and mitigation implementation should not assume a specific host to be the victim. Moreover, you should not have `h2` (or `h3`) specific details (such as IP address) hardcoded in your code.

#### Running the monitoring service

Once you have completed the TODOs, rerun the virtual environment (Part A)--with the new P4 `clone_packet` action and ONOS application---and the DNS reflection setup (Part B).

Let it run for a couple of minutes, and then start the monitoring service on `m1` by entering the following command in a separate shell:

```
$ cd assignments/assignment2
$ make start-monitor
```

After a couple of minutes, run `make plot-results` to generate a time-series of DNS response counts (`DNS_response_rates.png`).

> **Debugging tips:**
> - **Python**
>   - To debug your code in `start_dns_monitor.py`, you can print informative messages to stdout using print(message). These messages will be automatically redirected to the `m1_log.txt` file under the [`log`](log) folder. Please remove these prints before submitting these files. Stderr for `start_dns_monitor.py` will automatically print to `m1_err.txt`.
>   - Make sure you are checking whether a packet is a DNS packet before attempting to access its src/dst IP address or its DNS ID.
>   - If you start getting strange errors related to creating threads, restart the virtual environment.
> - **P4 & ONOS**
>   - To view if flow rules are successfully installed, run `make cli` to log into the ONOS controller. From there, you can enter `flows -s any` command to list the currently installed rules in switch `s1`.
>   - Similarly, you can check if the clone session (500 or 0x1F4 in hex) is active by entering `groups` command from the ONOS CLI.

### Part D: Analysis

Answer the questions in the file [`questions.txt`](questions.txt). Put your and your partner's names and Purdue emails at the top of the file.

## Submission and Grading
Submit the assignment by uploading your modified `start_dns_monitor.py`, `monitor.p4`, `PipelinerImpl.java`, `questions.txt`, and `DNS_response_rates.png` files to [Brightspace]().
You can submit as many times as you like before the deadline, but we will only take the last submission.
Each team only needs to submit once.

We will grade your assignments by running the simulation with your `start_dns_monitor.py`, `monitor.p4`, and `PipelinerImpl.java` files to test your DNS reflection detection and mitigation implementation. Double-check the specifications above and perform your own tests before submitting them.

## Acknowledgement

This assignment is modeled after a similar assignment offered at Princeton University by my Ph.D. advisor, Nick Feamster.
