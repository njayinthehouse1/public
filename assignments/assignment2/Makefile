############################################################################
##
##     This file is part of Purdue CS 536.
##
##     Purdue CS 536 is free software: you can redistribute it and/or modify
##     it under the terms of the GNU General Public License as published by
##     the Free Software Foundation, either version 3 of the License, or
##     (at your option) any later version.
##
##     Purdue CS 536 is distributed in the hope that it will be useful,
##     but WITHOUT ANY WARRANTY; without even the implied warranty of
##     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##     GNU General Public License for more details.
##
##     You should have received a copy of the GNU General Public License
##     along with Purdue CS 536. If not, see <https://www.gnu.org/licenses/>.
##
#############################################################################

####################################################################
############### Set up Mininet and Controller ######################
####################################################################

SCRIPTS = ../scripts

.PHONY: mininet controller cli netcfg host-h1 host-h2

mininet:
	$(SCRIPTS)/mn-stratum --custom net/topo.py --topo testtopo

controller:
	ONOS_APPS=gui,proxyarp,drivers.bmv2,lldpprovider,hostprovider,fwd  \
	$(SCRIPTS)/onos

cli:
	$(SCRIPTS)/onos-cli

netcfg:
	$(SCRIPTS)/onos-netcfg cfg/netcfg.json

host-h1:
	$(SCRIPTS)/utils/mn-stratum/exec h1

host-h2:
	$(SCRIPTS)/utils/mn-stratum/exec h2

host-h3:
	$(SCRIPTS)/utils/mn-stratum/exec h3

host-h4:
	$(SCRIPTS)/utils/mn-stratum/exec h4

host-m1:
	$(SCRIPTS)/utils/mn-stratum/exec m1

####################################################################
################### Build P4 and ONOS App ##########################
####################################################################

APP_OAR = app/target/forward-1.0-SNAPSHOT.oar
APP_BMV2 = app/src/main/resources/bmv2

build-p4: clean-p4
	mkdir -p .build-p4/bmv2 app/src/main/resources
	$(SCRIPTS)/p4c p4c-bm2-ss --arch v1model -o .build-p4/bmv2/bmv2.json \
		-DTARGET_BMV2 -DCPU_PORT=255 \
		--p4runtime-files .build-p4/bmv2/p4info.txt \
		p4src/main.p4
	# Copy the relevant files to the pipeconf resources
	cp -r .build-p4/bmv2 app/src/main/resources
	echo "255" > app/src/main/resources/bmv2/cpu_port.txt
	rm -rf .build-p4

$(APP_BMV2):
	$(error Missing bmv2 files, run 'make build-p4' first)

build-app: $(APP_BMV2) clean-app
	cd app && ../$(SCRIPTS)/maven clean package

$(APP_OAR):
	$(error Missing app binary, run 'make build-app' first)

reload-app: $(APP_OAR)
	$(SCRIPTS)/onos-app reinstall! $(APP_OAR)

####################################################################
########################## Start Net ###############################
####################################################################

install-utils:
	docker exec -it mn-stratum bash -c \
		"apt-get update ; \
		 apt-get -y --allow-unauthenticated install bind9 dnsutils python-scapy python-requests"

start-net: install-utils \
		   config-hosts \
		   start-dns \
		   start-victim-host \
		   start-regular-host \
		   start-attacker

config-hosts:
	for i in 1 2 3 4 ; do \
        $(SCRIPTS)/utils/mn-stratum/exec-script h$$i \
        	"sysctl -w net.ipv6.conf.all.disable_ipv6=1 && \
        	 sysctl -w net.ipv6.conf.default.disable_ipv6=1 && \
        	 sysctl -w net.ipv6.conf.lo.disable_ipv6=1"; \
    done

start-dns:
	# h1 is the domain name server
	$(SCRIPTS)/utils/mn-stratum/exec-d-script h1 \
		"/etc/init.d/bind9 stop &> /dev/null 2> /dev/null && \
		 /etc/init.d/bind9 start &> /dev/null 2> /dev/null"
	sleep 1

start-victim-host:
	# h2 is the victim
	$(SCRIPTS)/utils/mn-stratum/exec-d-script h2 \
		"python pysrc/legit_dns_traffic.py 10.0.0.1 5 > /dev/null & \
		 ping 10.0.0.1 -i 2 > log/h2_ping.txt & \
		 python pysrc/test_dns_traffic.py h2-eth0 00:00:00:00:00:02 > log/h2_test.txt &"
	sleep 1

start-regular-host:
	# h3 is a regular host
	$(SCRIPTS)/utils/mn-stratum/exec-d-script h3 \
		"python pysrc/legit_dns_traffic.py 10.0.0.1 5 > /dev/null & \
		 ping 10.0.0.1 -i 2 > log/h3_ping.txt & \
		 python pysrc/test_dns_traffic.py h3-eth0 00:00:00:00:00:03 > log/h3_test.txt &"
	sleep 1

start-attacker:
	# h4 is the attacker
	$(SCRIPTS)/utils/mn-stratum/exec-d-script h4 \
		"python pysrc/send_dns_query.py 10.0.0.2 10.0.0.1 0.5 > /dev/null"

start-monitor:
	# m1 is the monitor, which interacts with ONOS
	$(SCRIPTS)/utils/mn-stratum/exec-script m1 \
		"cd pysrc && python start_dns_monitor.py > ../log/m1_log.txt 2> ../log/m1_err.txt"

plot-results:
	$(SCRIPTS)/python pysrc/plot_dns_results.py --list log/h2_test.txt log/h3_test.txt --output log/dns_response_rates.png


clean: clean-logs clean-p4 clean-app clean-ctlr

clean-logs:
	rm -rf log/*

clean-p4:
	rm -rf app/src/main/resources/*

clean-app:
	rm -rf app/target

clean-ctlr:
	rm -rf ctlr/__pycache__